import { MatrixClient, MentionPill, MessageEvent, MessageEventContent } from "matrix-bot-sdk";
import * as htmlEscape from "escape-html";
const axios = require('axios')

export async function runSalvaiCommand(roomId: string, original_event: any, event: MessageEvent<MessageEventContent>, args: string[], client: MatrixClient) {
    // The first argument is always going to be us, so get the second argument instead.
    // let sayHelloTo = args[1];
    // if (!sayHelloTo) sayHelloTo = event.sender;

    // let text = `Hello ${sayHelloTo}!`;
    // let html = `Hello ${htmlEscape(sayHelloTo)}!`;
    // if (sayHelloTo.startsWith("@")) {
    //     // Awesome! The user supplied an ID so we can create a proper mention instead
    //     const mention = await MentionPill.forUser(sayHelloTo, roomId, client);
    // }

    // makes an axios post request to https://fractopia.pagekite.me/api/saved with the original_content
    try {
        let content = original_event?.content?.body
        console.log('events', original_event, event, event.sender)

        let response = await axios.post('http://0.0.0.0:3013/api/saved', {
            original_content: content
        })
        let text = `Salvei pra vc!`;
        let html = `Salvei pra vc!`;

        return client.sendMessage(roomId, {
            body: text,
            msgtype: "m.notice",
            format: "org.matrix.custom.html",
            formatted_body: html,
        });
    } catch (error) {
        console.error(error)
        let text = `Desculpa, mas acho que deu errado!`;
        let html = `Desculpa, mas acho que deu errado`;

        return client.sendMessage(roomId, {
            body: text,
            msgtype: "m.notice",
            format: "org.matrix.custom.html",
            formatted_body: html,
        });
    }



}
